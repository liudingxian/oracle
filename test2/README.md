### 实验2：用户及权限管理
学号：202010414409 姓名：刘定贤
#### 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制

#### 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

#### 实验步骤

![系统关系](./系统关系.jpg)

- 第一步：以system登录到pdborcl，创建角色con_res_ldx和用户ldx_sale，并授权和分配空间

1. 登录system：sqlplus system/123@pdborcl

2. 创建角色：create role con_res_ldx;

3. 进行授权：GRANT connect,resource,CREATE VIEW TO con_res_ldx;

4. 创建用户：CREATE USER ldx_sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;

5. 修改用户属性：ALTER USER ldx_sale default TABLESPACE "USERS";ALTER USER ldx_sale QUOTA 50M ON users;

![sale授权之前](./sale授权之前.png)

6. 授予权限：GRANT con_res_ldx TO ldx_sale;

 ![sale授权之后](./sale授权之后.JPG)  

7. 收回角色：REVOKE con_res_ldx FROM ldx_sale;

- 第二步：新用户ldx_sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

1. 新用户链接到pdborcl：sqlplus ldx_sale/123@pdborcl
2. 展示当前用户,查看session_privs，session_roles

```
SQL> show user;
USER 为 "LDX_SALE"
SQL> select * from session_privs;

PRIVILEGE
----------------------------------------
SET CONTAINER
CREATE INDEXTYPE
CREATE OPERATOR
CREATE TYPE
CREATE TRIGGER
CREATE PROCEDURE
CREATE SEQUENCE
CREATE VIEW
CREATE CLUSTER
CREATE TABLE
CREATE SESSION

已选择 11 行。

SQL> select * from session_roles;

ROLE
--------------------------------------------------------------------------------
CONNECT
RESOURCE
SODA_APP
CON_RES_LDX

```

3. 创建表customers，并插入数据

```
SQL> CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;

表已创建。

INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');

已创建 1 行。

SQL> 
已创建 1 行。

```

4. 创建视图customers_view并插入数据。

```
SQL> CREATE VIEW customers_view AS SELECT name FROM customers;

视图已创建。

```
![授权之前的hr](./授权给hr之前的查询.JPG)
5. 将customers_view的SELECT对象权限授予hr用户

```
SQL> GRANT SELECT ON customers_view TO hr;

授权成功。
```
![授权之后的hr](./授权之后的hr.JPG)
- 第三步：用户hr连接到pdborcl，查询sale授予它的视图customers_view

```
[oracle@oracle1 ~]$ sqlplus hr/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 16:55:29 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期二 4月  18 2023 16:52:29 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> SELECT * FROM ldx_sale.customers;
SELECT * FROM ldx_sale.customers
                       *
第 1 行出现错误:
ORA-00942: 表或视图不存在


SQL> SELECT * FROM ldx_sale.customers_view;

NAME
--------------------------------------------------
zhang
wang
```
![hr查询视图](./hr查询视图.JPG)
sale用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。

####  概要文件设置,用户最多登录时最多只能错误3次

```
[oracle@oracle1 ~]$ sqlplus system/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 16:58:11 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期二 4月  18 2023 16:38:23 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;

配置文件已更改

SQL> 
```
![三次错误](./3次尝试错误图.JPG)
登录system用户并且对相关表进行解锁：alter user ldx_sale  account unlock;

```
SQL> alter user ldx_sale  account unlock;

用户已更改。
```
![解锁图](./解锁图.JPG)
### 数据库和表空间占用分析

```
SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

TABLESPACE_NAME
------------------------------
FILE_NAME
--------------------------------------------------------------------------------
	MB                                                          MAX_MB           AUT
---------- ---------- ---
USERS/home/oracle/app/oracle/oradata/orcl/pdborcl/users01.dbf  5 32767.9844      YES

```

```
SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;

```
![数据库大小](./数据库大小.JPG)
#### 删除用户和角色

```
SQL> drop role con_res_ldx;

角色已删除。

SQL> drop user ldx_sale cascade;

用户已删除。
```

### 结论

本次实验我熟悉了orcal新建/注销用户和角色的方法，同时对数据库的权限管理和用户管理更加熟悉。
还有对应分配表空间权限和表空间限额有一点的了解。为以后的数据库开发和管理提高了基础
通过该实验我掌握了Oracle中系统权限和对象权限的概念，理解角色的基本概念，掌握了用户权限的授予与回收使用角色进行权限的授予与回收等基本语法。

在授予用户权限的时，系统权限和对象权限要分开授予。

在授予用户对象权限或者收回用户对象权限的时候，不会立即生效，需要等到下一次重新连接的时候才有效。

在收回用户系统权限时，默认不会级联收回，在收回用户对象权限的时候，默认会级联收回。


