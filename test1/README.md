2020级软件工程4班 202010414409 刘定贤
## 实验1：SQL语句的执行计划分析与优化指导
### 实验目的
分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

### 实验数据库和用户
数据库是pdborcl，用户是sys和hr

###实验内容

对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。
### 参考

用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：


 v_$sesstat, v_$statname 和 v_$session



权限分配过程如下
```
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr;
``` 
### 实验内容
1. 新建用户，授予权限
[oracle@oracle1 ~]$ sqlplus sys/123@localhost/pdborcl as sysdba

SQL*Plus: Release 12.2.0.1.0 Production on 星期一 3月 20 09:10:12 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.


连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> exit
从 Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production 断开
[oracle@oracle1 ~]$ sqlplus sys/123@localhost/pdborcl as sysdba @$ORACLE_HOME.sqlplus/admain/plustrce.sql

SQL*Plus: Release 12.2.0.1.0 Production on 星期一 3月 20 09:11:11 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.


连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SP2-0310: 无法打开文件 "/home/oracle/app/oracle/product/12.2.0/dbhome_1.sqlplus/admain/plustrce.sql"
SQL> create rple plustrace
  2  ;
create rple plustrace
       *
第 1 行出现错误:
ORA-00901: 无效 CREATE 命令


SQL> create role plustrace;

角色已创建。

SQL> grant select on v_$sesstat to plustrace
  2  ;

授权成功。

SQL> grant select on v_$statname to plustrace;

授权成功。

SQL> grant select on v_$mystat to plustrace;

授权成功。

SQL> grant plustrace to dba with admin option
  2  ;

授权成功。

SQL> grant plustrace to hr;

授权成功。

SQL> grant select on v_$sql to hr;

授权成功。

SQL> grant select on v_$sql_plan to hr;

授权成功。

SQL> grant select on v_$sql_plan_statistics_all to hr;

授权成功。

SQL> grant select on v_$session to hr;

授权成功。

SQL> grant select on v_$parameter to hr;

授权成功。

2. 查询employee里面，工资在10000以下并且部门是 'IT'和'Sales' 的部门名字和平均工资
- 查询1
```
set autotrace on
select d.department_name,avg(e.salary) 
from hr.departments d,hr.employees e 
where d.department_id = e.department_id
and d.department_name in ('IT','Sales') and e.salary<10000
group by d.department_name ;

```
DEPARTMENT_NAME 	       AVG(E.SALARY)

------------------------------ -------------

IT					5760
Sales				  7847.82609


执行计划
----------------------------------------------------------

Plan hash value: 3808327043

--------------------------------------------------------------------------------

-------------------

| Id  | Operation		      | Name		  | Rows  | Bytes | Cost
 (%CPU)| Time	  |

--------------------------------------------------------------------------------

-------------------

|   0 | SELECT STATEMENT	      | 		  |	1 |    23 |
5  (20)| 00:00:01 |

|   1 |  HASH GROUP BY		      | 		  |	1 |    23 |
5  (20)| 00:00:01 |

|   2 |   NESTED LOOPS		      | 		  |	7 |   161 |
4   (0)| 00:00:01 |

|   3 |    NESTED LOOPS 	      | 		  |    20 |   161 |
4   (0)| 00:00:01 |

|*  4 |     TABLE ACCESS FULL	      | DEPARTMENTS	  |	2 |    32 |
3   (0)| 00:00:01 |

|*  5 |     INDEX RANGE SCAN	      | EMP_DEPARTMENT_IX |    10 |	  |
0   (0)| 00:00:01 |

|*  6 |    TABLE ACCESS BY INDEX ROWID| EMPLOYEES	  |	3 |    21 |
1   (0)| 00:00:01 |

--------------------------------------------------------------------------------

-------------------


Predicate Information (identified by operation id):
---------------------------------------------------

   4 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
   6 - filter("E"."SALARY"<10000)

Note
-----

   - this is an adaptive plan
统计信息
 58  recursive calls
  0  db block gets
 22  consistent gets
  1  physical reads
  0  redo size
733  bytes sent via SQL*Net to client
608  bytes received via SQL*Net from client
  2  SQL*Net roundtrips to/from client
  0  sorts (memory)
  0  sorts (disk)
  2  rows processed


sqldever用时0.009s

- 查询2
```
set autotrace on
select d.department_name,avg(e.salary) 
from hr.departments d,hr.employees e 
where d.department_id = e.department_id
and d.department_name = 'IT' and e.salary<10000 or d.department_name= 'Sales' 
group by d.department_name ;

```
DEPARTMENT_NAME 	       AVG(E.SALARY)

------------------------------ -------------

IT					5760
Sales				  6461.83178


执行计划
----------------------------------------------------------

Plan hash value: 2646959616

--------------------------------------------------------------------------------

---------------------

| Id  | Operation			| Name		    | Rows  | Bytes | Co
st (%CPU)| Time     |

--------------------------------------------------------------------------------

---------------------

|   0 | SELECT STATEMENT		|		    |	110 |  3300 |
 11  (10)| 00:00:01 |

|   1 |  HASH GROUP BY			|		    |	110 |  3300 |
 11  (10)| 00:00:01 |

|   2 |   VIEW				| VW_ORE_19FF4E3E   |	110 |  3300 |
 10   (0)| 00:00:01 |

|   3 |    UNION-ALL			|		    |	    |	    |
	 |	    |

|   4 |     MERGE JOIN CARTESIAN	|		    |	107 |  1712 |
  6   (0)| 00:00:01 |

|*  5 |      TABLE ACCESS FULL		| DEPARTMENTS	    |	  1 |	 12 |
  3   (0)| 00:00:01 |

|   6 |      BUFFER SORT		|		    |	107 |	428 |
  3   (0)| 00:00:01 |

|   7 |       TABLE ACCESS FULL 	| EMPLOYEES	    |	107 |	428 |
  3   (0)| 00:00:01 |

|   8 |     NESTED LOOPS		|		    |	  3 |	 69 |
  4   (0)| 00:00:01 |

|   9 |      NESTED LOOPS		|		    |	 10 |	 69 |
  4   (0)| 00:00:01 |

|* 10 |       TABLE ACCESS FULL 	| DEPARTMENTS	    |	  1 |	 16 |
  3   (0)| 00:00:01 |

|* 11 |       INDEX RANGE SCAN		| EMP_DEPARTMENT_IX |	 10 |	    |
  0   (0)| 00:00:01 |

|* 12 |      TABLE ACCESS BY INDEX ROWID| EMPLOYEES	    |	  3 |	 21 |
  1   (0)| 00:00:01 |

--------------------------------------------------------------------------------

---------------------


Predicate Information (identified by operation id):
---------------------------------------------------

   5 - filter("D"."DEPARTMENT_NAME"='Sales')
  10 - filter("D"."DEPARTMENT_NAME"='IT' AND LNNVL("D"."DEPARTMENT_NAME"='Sales'
))

  11 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
  12 - filter("E"."SALARY"<10000)

Note
-----

   - this is an adaptive plan


统计信息
----------------------------------------------------------

	 22  recursive calls
	  0  db block gets
	 29  consistent gets
	  0  physical reads
	  0  redo size
	733  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

sqldever用时0.006s；


### 结果分析
分析：从结果来看，第二条查询语句更优，有结论 表连接>exist>not exist>in>not in;

我们知道SQL 数据库常见的优化手段分为三个层面：SQL 和索引优化、数据库结构优化、系统硬件优化等，然而每个大的方向中又包含多个小的优化点，下面我们具体来看看。
这里我们 d.department_name in ('IT','Sales')
我们将其修改会怎么样
   Oracle优化器在处理带IN的目标SQL时，通常会采用这四种方式，

1. 使用IN-List Iterator。

2. 使用IN-List Expansion。

3. 使用IN-List Filter。

4. 对IN做子查询展开/视图合并
本题采用 对IN做子查询展开/视图合并 进行的优化

