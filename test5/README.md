## 实验5：包，过程，函数的用法
### 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

### 实验内容

- 以hr用户登录


1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```
### 实验过程
1. 登录hr用户（sqlplus hr/123@pdborcl）
这个过程可用省略，因为我直接在Oracle SQL Developer里面选择的这个
2. 创建Mypack包
```
CREATE OR REPLACE PACKAGE MyPack AS
-- 在这里声明函数和过程
END MyPack;
/

```
```
CREATE OR REPLACE PACKAGE MyPack AS
  FUNCTION Get_SalaryAmount(departmentID NUMBER) RETURN NUMBER;
  PROCEDURE GET_EMPLOYEES(employeeID NUMBER);
END MyPack;

```
![创建包](./创建包.JPG)
3. 因为需要在创建包时候加入函数，我们开始创建函数
- 创建一个函数Get_SalaryAmount

```
CREATE OR REPLACE FUNCTION Get_SalaryAmount(p_department_id NUMBER) RETURN NUMBER IS
  v_salary_amount NUMBER;
BEGIN
  SELECT SUM(salary) INTO v_salary_amount
  FROM employees
  WHERE department_id = p_department_id;
  RETURN v_salary_amount;
END Get_SalaryAmount;
/
```
![创建函数](./创建函数.JPG)
- 创建存储过程
```
CREATE OR REPLACE PROCEDURE GET_EMPLOYEES(p_employee_id NUMBER) IS
  -- 声明变量
  v_employee_id employees.employee_id%TYPE := p_employee_id;
  v_first_name employees.first_name%TYPE;
  v_manager_id employees.manager_id%TYPE;
BEGIN
  -- 定义游标
  FOR employee IN (
    SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID
    FROM employees 
    START WITH EMPLOYEE_ID = v_employee_id
    CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
  ) LOOP
    v_first_name := employee.FIRST_NAME;
    v_manager_id := employee.MANAGER_ID;
    -- 输出格式：员工ID、姓名、上级员工ID、级别
    DBMS_OUTPUT.PUT_LINE(employee.EMPLOYEE_ID || ', ' || v_first_name || ', ' || v_manager_id || ', ' || employee.LEVEL);
  END LOOP;
END GET_EMPLOYEES;

```
![创建存储过程](./创建存储过程.JPG)

![创建目录](./完成创建后的目录.JPG)
4. 执行函数和过程
```
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;


```
![测试get_salaryAmount](./测试get_salaryAmount.JPG)

```
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/

```
![测试存储过程](./测试函数.JPG)

5. 整体创建代码：

首先，需要在Oracle数据库中创建一个包(Package)来存储函数和过程。下面是创建名为MyPack的包的SQL语句：

```
CREATE OR REPLACE PACKAGE MyPack AS
  FUNCTION Get_SalaryAmount(departmentID NUMBER) RETURN NUMBER;
  PROCEDURE GET_EMPLOYEES(employeeID NUMBER);
END MyPack;
```
接下来，在包内部定义函数Get_SalaryAmount，用于查询每个部门的薪资总额：

```
CREATE OR REPLACE PACKAGE BODY MyPack AS
  FUNCTION Get_SalaryAmount(departmentID NUMBER) RETURN NUMBER IS
    totalSalary NUMBER := 0;
  BEGIN
    SELECT SUM(salary)
    INTO totalSalary
    FROM employees
    WHERE department_id = departmentID;
    
    RETURN totalSalary;
  END Get_SalaryAmount;
  
  PROCEDURE GET_EMPLOYEES(employeeID NUMBER) IS
    cursor emp_cursor(employee_id NUMBER) IS 
      SELECT * FROM employees WHERE manager_id = employee_id;
      
    emp_rec employees%ROWTYPE;
  BEGIN
    -- 输出当前员工信息
    SELECT *
    INTO emp_rec
    FROM employees
    WHERE employee_id = employeeID;
    
    dbms_output.put_line(emp_rec.employee_id || ' ' || emp_rec.first_name || ' ' || emp_rec.last_name);
    
    -- 遍历所有下属员工
    FOR sub_emp IN emp_cursor(employeeID) LOOP
      GET_EMPLOYEES(sub_emp.employee_id); -- 递归调用自己
    END LOOP;
  END GET_EMPLOYEES;
END MyPack;
```
在上述代码中，我们首先定义了一个游标emp_cursor，用于查询某个经理管理的所有员工。然后定义了一个emp_rec用于存储查询结果的行记录。在过程GET_EMPLOYEES中，我们使用SELECT INTO语句将当前员工的信息输出，然后遍历他的所有下属员工，递归调用自己。

在Oracle数据库中，需要使用dbms_output.put_line函数来输出结果。可以通过执行以下SQL语句打开dbms_output输出：

```
SET SERVEROUTPUT ON;
```
### 总结
1. Oracle的包用于在逻辑上组合过程和函数。包由包规范和包体两部分组成（包规范即包定义。是用来定义方法、过程等的。）

基本语法
```
CREATE [OR REPLACE] PACKAGE 包名 IS
PROCEDURE 过程名(变量名 变量类型);
FUNCTION 函数名(变量名 变量类型)  RETURN 数据类型;
END
```
2. 包体是用来实现包中定义的方法和过程的。包规范定义完成后，便可以调用。但是没有包体的时候，定义的方法和过程是没有内容的。直接调用会报错
包体基本语法
```
CREATE OR REPLACE PACKAGE BODY 包名 IS
PROCEDURE 过程名(变量名 变量类型) IS
BEGIN
执行语句
END;
FUNCTION 函数名(变量名 变量类型)
RETURN 数据类型 IS
定义变量;
BEGIN
执行语句;
END;
END;
```

3. **调用包的方法：方案名.包名.过程名或函数名(参数值);**

本次项目里面，我是先单独创建了一个存储过程和函数，然后再写的包的方法体。（因为最开始理解错了题目，后面报错了才发现我包里面的函数只有一个名字，所以后面整体创建代码是ok的）。

通过本次实验，了解了函数、存储过程、包的创建和实验方法，并且能够调用相关的方法。

