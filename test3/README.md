## 实验3：创建分区表
学号：202010414409 姓名：刘定贤
### 实验目的
掌握分区表的创建方法，掌握各种分区方式的使用场景。
### 实验内容
- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
进行分区与不分区的对比实验。
### 实验步骤
就是用我们新建的用户来创建数据库
1. 按照实验2完成sale_ldx和订单表，订单详表的创建
**重点**：使用我们创建的对象进行登录

```
--------------------------------------------------------
--  文件已创建 - 星期二-四月-25-2023   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ORDERTABLE
--------------------------------------------------------

  CREATE TABLE "SALE_LDX"."ORDERS" 
   (	"ORDER_ID" NUMBER(9,0), 
	"ORDER_NAME" VARCHAR2(20 BYTE), 
	"COLUMN1" DATE, 
	"EMPLOYEE_ID" NUMBER(6,0), 
	"DISCOUNT" NUMBER(8,2) DEFAULT 0, 
	"TRADE_RECEIVABLE" NUMBER(8,2) DEFAULT 0
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SYSAUX" ;
REM INSERTING into SALE_LDX.ORDER_DETAIL
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index ORDERTABLE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SALE_LDX"."ORDERS_PK" ON "SALE_LDX"."ORDERS" ("ORDER_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX" ;
--------------------------------------------------------
--  Constraints for Table ORDERTABLE
--------------------------------------------------------

  ALTER TABLE "SALE_LDX"."ORDERS"   MODIFY ("ORDER_ID" NOT NULL ENABLE);
  ALTER TABLE "SALE_LDX"."ORDERS"   ("ORDER_NAME" NOT NULL ENABLE);
  ALTER TABLE "SALE_LDX"."ORDERS"  ("COLUMN1" NOT NULL ENABLE);
  ALTER TABLE "SALE_LDX"."ORDERS"   ("EMPLOYEE_ID" NOT NULL ENABLE);
  ALTER TABLE "SALE_LDX"."ORDERS"  ADD CONSTRAINT "ORDERS_PK" PRIMARY KEY ("ORDER_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX"  ENABLE;

```

```
--------------------------------------------------------
--  文件已创建 - 星期二-四月-25-2023   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ORDER_DETAIL
--------------------------------------------------------

  CREATE TABLE "SALE_LDX"."ORDER_DETAIL" 
   (	"ID" NUMBER(9,0), 
	"ORDER_ID" NUMBER(10,0), 
	"PRODUCT_ID" VARCHAR2(40 BYTE), 
	"PRODUCT_NUM" NUMBER(8,2), 
	"PRODUCT_PRICE" NUMBER(8,2)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SYSAUX" ;

   COMMENT ON COLUMN "SALE_LDX"."ORDER_DETAIL"."ORDER_ID" IS 'poreing key';
REM INSERTING into SALE_LDX.ORDER_DETAIL
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index ORDER_DETAIL_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SALE_LDX"."ORDER_DETAIL_PK" ON "HR"."ORDER_DETAIL" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX" ;
--------------------------------------------------------
--  Constraints for Table ORDER_DETAIL
--------------------------------------------------------

  ALTER TABLE "SALE_LDX"."ORDER_DETAIL" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "SALE_LDX"."ORDER_DETAIL" MODIFY ("ORDER_ID" NOT NULL ENABLE);
  ALTER TABLE "SALE_LDX"."ORDER_DETAIL" MODIFY ("PRODUCT_ID" NOT NULL ENABLE);
  ALTER TABLE "SALE_LDX"."ORDER_DETAIL" MODIFY ("PRODUCT_NUM" NOT NULL ENABLE);
  ALTER TABLE "SALE_LDX"."ORDER_DETAIL" MODIFY ("PRODUCT_PRICE" NOT NULL ENABLE);
  ALTER TABLE "SALE_LDX"."ORDER_DETAIL" ADD CONSTRAINT "ORDER_DETAIL_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX"  ENABLE;

```
2. 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。

-  打开Oracle SQL Developer，并连接到相应的数据库。
- 在对象浏览器中，展开所需表所在的模式节点，右键单击该表并选择“编辑”选项。
- 单击“约束”选项卡，然后单击“添加”按钮以打开“新建约束”对话框。
- 在“新建约束”对话框中，输入要创建的外键的名称和相关列。
- 选择“外键”类型并单击“确定”按钮，以保存新创建的外键约束。

![外键](./外键.JPG)
3. 新建两个序列，分别设置ordertable.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。

```
<!-- 新建两个序列 -->
CREATE SEQUENCE order_seq MINVALUE 1 MAXVALUE 999999999 START WITH 1 INCREMENT BY 1 NOCACHE
NOCYCLE;
CREATE SEQUENCE order_detail_seq MINVALUE 1 MAXVALUE 999999999 START WITH 1 INCREMENT BY 1 NOCACHE
NOCYCLE;
<!-- 设置两个触发器 -->
create or replace trigger menu_autoinc_tg
before insert on ordertable for each row
begin
select order_detail_seq.nextval into :new.Order_id from dual;
end;

create or replace trigger detail_tgd
before insert on order_detail for each row
begin
select order_seq.nextval into :new.detail_id from dual;
end;
<!-- 做插入测试 -->
INSERT INTO order_detail (order_id, product_id, product_num, product_name,product_price) 
VALUES (
    1, -- 自增序列nextval函数获取order_id
    3001,                      -- product_id
    20,                        -- product_num
    'product_name',            -- 产品名称
    28.12                      -- 价格
); 

insert into ordertable(order_name,column1,Employee_id,Discount,trade_receivable)
values('zs',to_date('1987-04-19','yyyy-mm-dd'),1002,2001,90);
```
![给order添加触发器并测试](./给order添加触发器并测试.jpg)
![第二个触发器和插入测试](./第二个触发器和插入测试.jpg)
![测试数据1](./测试数据1.jpg)
![测试数据2](./测试数据2.jpg)

4. ordertable表按订单日期（order_date）设置范围分区。
```
PARTITION BY RANGE (ORDER_DATE)
(
  PARTITION PARTITION_2015 VALUES LESS THAN (TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    INITIAL 8388608
    NEXT 1048576
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2016 VALUES LESS THAN (TO_DATE(' 2017-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2017 VALUES LESS THAN (TO_DATE(' 2018-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2018 VALUES LESS THAN (TO_DATE(' 2019-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS02
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2019 VALUES LESS THAN (TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS02
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2020 VALUES LESS THAN (TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS02
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2021 VALUES LESS THAN (TO_DATE(' 2022-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS03
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
);

```
5. 整体sql创建语句：
```
#建立ORDERS表并分区
CREATE TABLE ORDERS
(
  ORDER_ID NUMBER(10, 0) NOT NULL
, CUSTOMER_NAME VARCHAR2(40 BYTE) NOT NULL
, CUSTOMER_TEL VARCHAR2(40 BYTE) NOT NULL
, ORDER_DATE DATE NOT NULL
, EMPLOYEE_ID NUMBER(6, 0) NOT NULL
, DISCOUNT NUMBER(8, 2) DEFAULT 0
, TRADE_RECEIVABLE NUMBER(8, 2) DEFAULT 0
, CONSTRAINT ORDERS_PK PRIMARY KEY
  (
    ORDER_ID
  )
  USING INDEX
  (
      CREATE UNIQUE INDEX ORDERS_PK ON ORDERS (ORDER_ID ASC)
      LOGGING
      TABLESPACE USERS
      PCTFREE 10
      INITRANS 2
      STORAGE
      (
        BUFFER_POOL DEFAULT
      )
      NOPARALLEL
  )
  ENABLE
)
TABLESPACE USERS
PCTFREE 10
INITRANS 1
STORAGE
(
  BUFFER_POOL DEFAULT
)
NOCOMPRESS
NOPARALLEL
PARTITION BY RANGE (ORDER_DATE)
(
  PARTITION PARTITION_2015 VALUES LESS THAN (TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    INITIAL 8388608
    NEXT 1048576
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2016 VALUES LESS THAN (TO_DATE(' 2017-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2017 VALUES LESS THAN (TO_DATE(' 2018-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2018 VALUES LESS THAN (TO_DATE(' 2019-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS02
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2019 VALUES LESS THAN (TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS02
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2020 VALUES LESS THAN (TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS02
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2021 VALUES LESS THAN (TO_DATE(' 2022-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS03
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
);

#建立order_details表并建立引用分区
CREATE TABLE order_details
(
id NUMBER(10, 0) NOT NULL
, order_id NUMBER(10, 0) NOT NULL
, product_name VARCHAR2(40 BYTE) NOT NULL
, product_num NUMBER(8, 2) NOT NULL
, product_price NUMBER(8, 2) NOT NULL
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
)
TABLESPACE USERS
PCTFREE 10 INITRANS 1
STORAGE (BUFFER_POOL DEFAULT )
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);

```
6. 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）
写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
![批量插入](./批量插入.jpg)
```
INSERT INTO orders (order_id, order_date)
SELECT rownum, sysdate-200000+rownum
FROM dual
CONNECT BY level <= 100000;


INSERT INTO order_detail (order_id,product_price)
SELECT  rownum,round(dbms_random.value(100,10000))
FROM dual
CONNECT BY level <= 100000;

```
![数据](./数据.jpg)


7. 进行分区与不分区的对比实验。

**无分区**

```

CREATE TABLE ORDERS_NOSPACE 
(
  ORDER_ID NUMBER(10, 0) NOT NULL 
, CUSTOMER_NAME VARCHAR2(40 BYTE) NOT NULL 
, CUSTOMER_TEL VARCHAR2(40 BYTE) NOT NULL 
, ORDER_DATE DATE NOT NULL 
, EMPLOYEE_ID NUMBER(6, 0) DEFAULT 0 
, DISCOUNT NUMBER(8, 2) DEFAULT 0 
, CONSTRAINT ORDERS_ID_ORDERS_DETAILS PRIMARY KEY 
  (
    ORDER_ID 
  )
  USING INDEX 
  (
      CREATE UNIQUE INDEX ORDERS_ID_ORDERS_DETAILS ON ORDERS_NOSPACE (ORDER_ID ASC) 
      LOGGING 
      TABLESPACE USERS 
      PCTFREE 10 
      INITRANS 2 
      STORAGE 
      ( 
        BUFFER_POOL DEFAULT 
      ) 
      NOPARALLEL 
  )
  ENABLE 
) 
LOGGING 
TABLESPACE USERS 
PCTFREE 10 
INITRANS 1 
STORAGE 
( 
  BUFFER_POOL DEFAULT 
) 
NOCOMPRESS 
NO INMEMORY 
NOPARALLEL;

CREATE TABLE ORDER_DETAILS_NOSPACE 
(
  ID NUMBER(10, 0) NOT NULL 
, ORDER_ID NUMBER(10, 0) NOT NULL 
, PRODUCT_NAME VARCHAR2(40 BYTE) NOT NULL 
, PRODUCT_NUM NUMBER(8, 2) NOT NULL 
, PRODUCT_PRICE NUMBER(8, 2) NOT NULL 
) 
LOGGING 
TABLESPACE USERS 
PCTFREE 10 
INITRANS 1 
STORAGE 
( 
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  BUFFER_POOL DEFAULT 
) 
NOCOMPRESS 
NO INMEMORY 
NOPARALLEL;

ALTER TABLE ORDER_DETAILS_NOSPACE
ADD CONSTRAINT ORDERS_FOREIGN_ORDERS_DETAILS FOREIGN KEY
(
  ORDER_ID 
)
REFERENCES ORDERS_NOSPACE
(
  ORDER_ID 
)
ENABLE;

```
**有分区**
```
 select * from orders where order_date between to_date('2017-1-1','yyyy-mm-dd') and to_date('2021-6-1','yyyy-mm-dd');


Plan hash value: 3103369360

--------------------------------------------------------------------------------
-------------------

| Id  | Operation		 | Name   | Rows  | Bytes | Cost (%CPU)| Time
  | Pstart| Pstop |

--------------------------------------------------------------------------------
-------------------

|   0 | SELECT STATEMENT	 |	  |	1 |   105 |	2   (0)| 00:00:0
1 |	  |	  |

|   1 |  PARTITION RANGE ITERATOR|	  |	1 |   105 |	2   (0)| 00:00:0
1 |	3 |	7 |

|*  2 |   TABLE ACCESS FULL	 | ORDERS |	1 |   105 |	2   (0)| 00:00:0
1 |	3 |	7 |

--------------------------------------------------------------------------------
-------------------


Predicate Information (identified by operation id):
---------------------------------------------------

   2 - filter("ORDER_DATE"<=TO_DATE(' 2021-06-01 00:00:00', 'syyyy-mm-dd hh24:mi
:ss'))


Note
-----
   - dynamic statistics used: dynamic sampling (level=2)


```
```
select * from orders where order_date between to_date('2017-1-1','yyyy-mm-dd') and to_date('2017-6-1','yyyy-mm-dd');


Plan hash value: 3905487950

--------------------------------------------------------------------------------
-----------------

| Id  | Operation	       | Name	| Rows	| Bytes | Cost (%CPU)| Time
| Pstart| Pstop |

--------------------------------------------------------------------------------
-----------------

|   0 | SELECT STATEMENT       |	|     1 |   105 |     2   (0)| 00:00:01
|	|	|

|   1 |  PARTITION RANGE SINGLE|	|     1 |   105 |     2   (0)| 00:00:01
|     3 |     3 |

|*  2 |   TABLE ACCESS FULL    | ORDERS |     1 |   105 |     2   (0)| 00:00:01
|     3 |     3 |

--------------------------------------------------------------------------------
-----------------


Predicate Information (identified by operation id):
---------------------------------------------------

   2 - filter("ORDER_DATE"<=TO_DATE(' 2017-06-01 00:00:00', 'syyyy-mm-dd hh24:mi
:ss'))


Note
-----
   - dynamic statistics used: dynamic sampling (level=2)

SQL> 

```
### 总结
本次实验给我的感觉属于比较困难了。关于建表部分属于最简单部分，可用sql或者直接软件建表都ok，然后就是设置自动递增的主键，但是，Oracle设置自动递增和mysql不像，不过老师给了一个实例了，其实还ok，关于批量导入数据，老师给了一种，但是我自己对那种方式立即部署很深，所以换了一种。目前一个表插入了十万条数据。

通过分区和不分区的对比，有如下结论：
- 如果在分区表里查询数据，同一个分区查找明显比不同分区查找快。
- 在orders数据量为10000,order_details数据量为30000时，有分区比无分区查找数据优势更大。
如果数据量小，有分区与无分区差别不是很大，甚至无分区可能更快。
- 如果数据量大，分区表的优势明显加大。

在实验中我们还进行了不分区和分区的对比实验，结果显示，利用分区表可以提高数据存储和查询的效率，可以更加快速方便地进行数据处理。

总的来说，通过本次实验，我们深入了解了分区表的创建和使用方法，掌握了分区索引的使用技巧，提高了对分区表的运用能力。



