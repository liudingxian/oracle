# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

## 整体设计
表：
商品表（商品id、商品名字、商品价格、供应商、上架）
用户表（用户id，密码，手机号，地址，购买商品）
订单表（订单id、下单时间、订单状态）
订单详情表（订单id、商品id、用户id、商品数量）


权限分配：查看实验2
管理员用户：拥有对所有表的增删改查
普通用户：只能对订单表和订单详情表进行增删改查


表空间：
用户数据表空间：用于存放用户数据表、索引、视图等信息


包、函数、存储过程（实验5）
查询：某日购买了xx商品xx个（次）的用户名字
          select username from usertable where userid in(
                        select user_id from order_detail where 商品数量=x and
                        订单id in(
                           select 订单id from order_table where datetime=xx
)
  )
触发器：当下单成功，插入商品数据到logo表（或者日志表里面写入下单成功）
存储过程：通过商品id查询商品累计数量
                （select 库存 from 商品+select id，sum(商品数量) from 订单详情 group by 商品id）

数据库备份：查看随堂练习8、9

## 实验步骤
### 1. 创建用户和表空间
首先，我们需要创建两个不同的表空间，分别用于存放数据和索引。这样可以提高数据库查询效率。执行以下 SQL 语句即可创建新的表空间。
```
<!-- 创建订单详情的表空间 -->
CREATE TABLESPACE tbs_order_detail_index
DATAFILE '/home/oracle/database/tbs_order_detail_index'
SIZE 100M AUTOEXTEND ON;
<!-- 创建商品的表空间 -->
CREATE TABLESPACE tbs_order_detail_index
DATAFILE '/home/oracle/database/tbs_prod_index'
SIZE 100M AUTOEXTEND ON;
<!-- 创建订单的表空间 -->
CREATE TABLESPACE tbs_order_detail_index
DATAFILE '/home/oracle/database/tbs_orders_index'
SIZE 100M AUTOEXTEND ON;
<!-- 创建用户的表空间 -->
CREATE TABLESPACE tbs_order_detail_index
DATAFILE '/home/oracle/database/tbs_cust_index'
SIZE 100M AUTOEXTEND ON;
```
![表空间](./表空间.JPG)
为了保证系统的安全性，我们需要创建至少两个用户，一个用于管理系统数据的超级用户admin，一个用于普通用户操作的常规用户user1。超级用户拥有最高权限，可以对整个系统进行维护和操作，而常规用户只能处理一部分数据，不能擅自更改系统设置。我们可以使用以下SQL语句创建这两个用户：
```
<!-- 创建管理员用户 -->
CREATE USER admin IDENTIFIED BY admin_password;
GRANT CONNECT, RESOURCE, DBA TO admin;
<!-- 创建普通用户 -->
CREATE USER user1 IDENTIFIED BY user1_password;
GRANT CONNECT, RESOURCE TO user1;
```
通过以上SQL语句，我们创建了两个用户admin和user1，其中admin是超级用户，user1是常规用户。admin用户被授权CONNECT、RESOURCE和DBA的角色，即拥有全部数据库操作权限，而user1用户只被授予CONNECT和RESOURCE角色，意味着它只能执行一些基本的数据库操作。

### 2. 创建表
```
商品表：
CREATE TABLE products (
product_id NUMBER(10) PRIMARY KEY,
product_name VARCHAR2(50),
price NUMBER(10,2),
category VARCHAR2(50),
supplier VARCHAR2(50)
) TABLESPACE tbs_prod_index;

订单表：
CREATE TABLE orders (
order_id NUMBER(10) PRIMARY KEY,
customer_id NUMBER(10),
order_date DATE,
total_price NUMBER(10,2)
) TABLESPACE tbs_orders_index;

订单详情表:
CREATE TABLE order_details (
order_id NUMBER(10),
product_id NUMBER(10),
quantity NUMBER(10),
price NUMBER(10,2),
CONSTRAINT order_details_primary_key PRIMARY KEY (order_id, product_id)
) TABLESPACE tbs_order_detail_index;

用户表：
CREATE TABLE customers (
customer_id NUMBER(10) PRIMARY KEY,
customer_name VARCHAR2(50),
email VARCHAR2(50),
phone_number VARCHAR2(20),
address VARCHAR2(100)
) TABLESPACE tbs_cust_index;

库存表：
CREATE TABLE inventory (
product_id NUMBER(10) PRIMARY KEY,
stock_quantity NUMBER(10)
) TABLESPACE tbs_prod_index;
```
### 3.在order里面通过时间建立分区表
```
PARTITION BY RANGE (ORDER_DATE)
(
  PARTITION PARTITION_2015 VALUES LESS THAN (TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    INITIAL 8388608
    NEXT 1048576
    MINEXTENTS 1
    MAXEXTENTS UNLIMITED
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2016 VALUES LESS THAN (TO_DATE(' 2017-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2017 VALUES LESS THAN (TO_DATE(' 2018-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2018 VALUES LESS THAN (TO_DATE(' 2019-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS02
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2019 VALUES LESS THAN (TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS02
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2020 VALUES LESS THAN (TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS02
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
, PARTITION PARTITION_2021 VALUES LESS THAN (TO_DATE(' 2022-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
  NOLOGGING
  TABLESPACE USERS03
  PCTFREE 10
  INITRANS 1
  STORAGE
  (
    BUFFER_POOL DEFAULT
  )
  NOCOMPRESS NO INMEMORY
);
```
### 4.插入十万条数据
#### 4.1 商品表
```

-- 假设 product_name、category 和 supplier 列都需要填写
-- 产生一些假数据用于演示目的
DECLARE
    -- 可能的 product 名称和类别
    TYPE string_list IS VARRAY(20) OF VARCHAR2(50);
    products string_list := string_list(
        'Shampoo', 'Conditioner', 'Soap', 'Toothbrush', 'Toothpaste',
        'Mouthwash', 'Deodorant', 'Lotion', 'Sunscreen', 'Lip balm',
        'Hand soap', 'Body wash', 'Razor', 'Shaving cream', 'Brush',
        'Tweezers', 'Nail clippers', 'Hair gel', 'Hair spray', 'Pomade'
    );
    categories string_list := string_list(
        'Hair care', 'Oral care', 'Skin care', 'Shaving supplies', 'Tools'
    );
    
BEGIN
    -- 随机生成一个供应商
    FOR i IN 1 .. 100000 LOOP
        INSERT INTO products (
            product_id, product_name, price, category, supplier
        ) VALUES (
            i,
            products(TRUNC(DBMS_RANDOM.VALUE(1, 20))),
            DBMS_RANDOM.VALUE(0.99, 49.99),
            categories(TRUNC(DBMS_RANDOM.VALUE(1, 5))),
            'ACME Inc'
        );
        
        IF MOD(i, 1000) = 0 THEN
            COMMIT;
        END IF;
    END LOOP;
END;
```
![product数据](./product数据.JPG)
#### 4.2：订单表
```
INSERT INTO orders(order_id, customer_id, order_date, total_price)
SELECT level, mod(level, 10000)+1, DATE '2023-01-01' + mod(level, 2500), mod(level, 1000)+1
FROM dual
CONNECT BY level <= 100000;
```
![order数据](./order数据.JPG)
### 4.3 订单详情表
```
INSERT INTO order_details (order_id, product_id, quantity, price)
SELECT level, mod(level, 1000)+1, mod(level, 10)+1, mod(level, 100)+1
FROM dual
CONNECT BY level <= 100000;
```
![orderdetail数据](./orderdetail数据.JPG)
### 4.4 库存表
```
DECLARE
-- 可能的 product ID 范围
min_product_id NUMBER := 1;
max_product_id NUMBER := 100000;
BEGIN
FOR i IN min_product_id .. max_product_id LOOP
-- 随机生成一个库存量（范围为1~100）
INSERT INTO inventory (product_id, stock_quantity)
VALUES (i, TRUNC(DBMS_RANDOM.VALUE(1, 100)));

    IF MOD(i, 1000) = 0 THEN
        COMMIT;
    END IF;
END LOOP;
END;
```
![库存表数据](./库存表数据.JPG)

### 5.创建包，并在包里面设计一些存储过程和函数
我们创建一个程序包sales_pkg，其中包含以下存储过程和函数:

place_order：用于将客户订单保存到订单表中，并更新库存表中的库存数量。
cancel_order：用于取消客户订单，释放库存数量并删除订单表中的订单。
view_order_details：用于根据订单ID查看订单详情。
```
-- 创建程序包
CREATE OR REPLACE PACKAGE sales_pkg AS
    -- 存储过程1：将客户订单保存到订单表中，并更新库存表中的库存数量
    PROCEDURE place_order(
        p_product_id IN NUMBER,
        p_quantity IN NUMBER,
        p_customer_id IN NUMBER
    );

    -- 存储过程2：取消客户订单，释放库存数量并删除订单表中的订单
    PROCEDURE cancel_order(
        p_order_id IN NUMBER
    );

    -- 函数3：根据订单ID查看订单详情
    FUNCTION view_order_details(
        p_order_id IN NUMBER 
    ) RETURN SYS_REFCURSOR;
END sales_pkg;
/
```
创建 place_order 存储过程
```
CREATE OR REPLACE PROCEDURE place_order(
    p_product_id IN NUMBER,
    p_quantity IN NUMBER,
    p_customer_id IN NUMBER
)
IS
    v_order_id orders.order_id%TYPE;
    v_total_price orders.total_price%TYPE;
BEGIN
    -- 生成订单ID
    SELECT orders_seq.NEXTVAL INTO v_order_id FROM dual;

    -- 获取商品单价和当前库存数量
    DECLARE
        v_product_price products.price%TYPE;
        v_stock_quantity inventory.stock_quantity%TYPE;
    BEGIN
        SELECT price INTO v_product_price FROM products WHERE product_id = p_product_id;
        SELECT stock_quantity INTO v_stock_quantity FROM inventory WHERE product_id = p_product_id;
        
        -- 如果库存不足，则抛出异常
        IF p_quantity > v_stock_quantity THEN
            RAISE_APPLICATION_ERROR(-20001, 'Insufficient stock quantity!');
        END IF;
        
        -- 更新库存数量
        UPDATE inventory SET stock_quantity = stock_quantity - p_quantity WHERE product_id = p_product_id;
    END;

    -- 计算订单总价
    v_total_price := p_quantity * v_product_price;

    -- 在 orders 表中插入新的订单记录
    INSERT INTO orders (order_id, customer_id, order_date, total_price)
    VALUES (v_order_id, p_customer_id, SYSDATE, v_total_price);

    -- 在 order_details 表中插入新的订单详情记录
    INSERT INTO order_details (order_id, product_id, quantity, price)
    VALUES (v_order_id, p_product_id, p_quantity, v_product_price);
    
    -- 打印输出确认信息
    DBMS_OUTPUT.PUT_LINE('Order placed successfully! Order ID: ' || v_order_id);
EXCEPTION
    WHEN OTHERS THEN
        -- 如果有异常，回滚事务并抛出异常
        ROLLBACK;
        RAISE;
END;
/

```
创建 cancel_order 存储过程
```
CREATE OR REPLACE PROCEDURE cancel_order(
    p_order_id IN NUMBER
)
IS
    v_product_id order_details.product_id%TYPE;
    v_quantity order_details.quantity%TYPE;
BEGIN
    -- 获取订单详情信息
    SELECT product_id, quantity INTO v_product_id, v_quantity FROM order_details WHERE order_id = p_order_id;
    
    -- 更新库存数量
    UPDATE inventory SET stock_quantity = stock_quantity + v_quantity WHERE product_id = v_product_id;

    -- 从 order_details 表中删除订单详情记录
    DELETE FROM order_details WHERE order_id = p_order_id;
    
    -- 从 orders 表中删除订单记录
    DELETE FROM orders WHERE order_id = p_order_id;
    
    -- 打印输出确认信息
    DBMS_OUTPUT.PUT_LINE('Order cancelled successfully! Order ID: ' || p_order_id);
EXCEPTION
    WHEN OTHERS THEN
        -- 如果有异常，回滚事务并抛出异常
        ROLLBACK;
        RAISE;
END;
/

```
创建 view_order_details 函数
```
CREATE OR REPLACE FUNCTION view_order_details(
    p_order_id IN NUMBER
) RETURN SYS_REFCURSOR
IS
    v_cursor SYS_REFCURSOR;
BEGIN
    -- 返回所有与订单ID匹配的订单详情信息
    OPEN v_cursor FOR
        SELECT o.order_id, o.customer_id, o.order_date, od.product_id, p.product_name, od.quantity, od.price 
        FROM orders o
        JOIN order_details od ON o.order_id = od.order_id
        JOIN products p ON od.product_id = p.product_id
        WHERE o.order_id = p_order_id;
    RETURN v_cursor;
END;
/

```
在程序包sales_pkg中，我们定义了三个存储过程和一个函数，分别用于处理下单、取消订单和查看订单详情的业务逻辑。

![包存储过程函数目录](./包存储过程函数目录.JPG)

### 6.测试
测试 place_order 存储过程
```
BEGIN
  sales_pkg.place_order(p_product_id => 1001, p_quantity => 2, p_customer_id => 10001);
END;

SELECT *
FROM orders
WHERE order_id = (SELECT MAX(order_id) FROM orders);

SELECT *
FROM inventory
WHERE product_id = 1001;

```
第一个 SELECT 语句会返回最新插入的订单信息，包括订单 ID、产品 ID、数量、订单时间等等。

第二个 SELECT 语句会返回对应的库存信息，包括产品 ID、库存数量等等。

如果这两个查询都返回了符合预期的结果，说明存储过程已经成功地插入了一条订单并更新了库存。
![测试place](./测试place.JPG)

测试 cancel_order 存储过程
```
BEGIN
  sales_pkg.cancel_order(p_order_id => 123456);
END;
SELECT *
FROM orders
WHERE order_id = 123456;

SELECT *
FROM inventory
WHERE product_id = (SELECT product_id FROM order_items WHERE order_id = 123456);
```
第一个 SELECT 语句会返回被取消的订单信息，包括订单 ID、产品 ID、数量、订单状态等等。如果该订单不存在，则说明取消操作已经成功。

第二个 SELECT 语句会返回对应的库存信息，包括产品 ID、库存数量等等。如果这个查询返回了符合预期的结果，说明库存已经被成功释放。

请注意，在 cancel_order 存储过程中，有可能需要更新多个相关的表，以取消订单并释放库存。因此，你可能需要编写更复杂的 SQL 语句来检查操作结果。

![calse](./calse.JPG)
### 6.存储过程和函数
- 获取订单总价
```
CREATE OR REPLACE FUNCTION get_order_total_price(order_id IN NUMBER)
RETURN NUMBER
IS
  v_total_price NUMBER(10,2);
BEGIN
  FOR r IN (SELECT total_price FROM orders WHERE order_id = order_id)
  LOOP
    v_total_price := r.total_price;
  END LOOP;
  RETURN v_total_price;
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Error occurred: ' || SQLCODE || '-' || SQLERRM);
END;

<!-- 测试 -->

DECLARE
  v_order_id NUMBER := 123; -- 替换成实际的订单 ID
  v_total_price NUMBER(10,2);
BEGIN
  -- 调用存储过程获取订单总价
  v_total_price := get_order_total_price(v_order_id);

  -- 打印结果
  DBMS_OUTPUT.PUT_LINE('Order ' || v_order_id || ' total price is: $' || v_total_price);
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Error occurred: ' || SQLCODE || '-' || SQLERRM);
END;
```
![测试总价](./测试总价.JPG)

- 获取获取商品价格
```
CREATE OR REPLACE FUNCTION get_product_price(product_id IN NUMBER)
RETURN NUMBER
IS
  v_price products.price%TYPE;
BEGIN
  SELECT price INTO v_price FROM products WHERE product_id = product_id AND rownum = 1;
  RETURN v_price;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE('Product ' || product_id || ' not found');
    RETURN NULL;
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Error occurred: ' || SQLCODE || '-' || SQLERRM);
    RETURN NULL;
END;

<!-- 测试 -->
DECLARE
  v_product_id NUMBER := 123; -- 替换成实际的商品 ID
  v_price NUMBER(10,2);
BEGIN
  -- 调用函数获取商品价格
  v_price := get_product_price(v_product_id);

  -- 打印结果
  DBMS_OUTPUT.PUT_LINE('Product ' || v_product_id || ' price is: $' || v_price);
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Error occurred: ' || SQLCODE || '-' || SQLERRM);
END;
```
![getprice](./getprice.JPG)
- 下单
```
CREATE OR REPLACE PROCEDURE place_order(
p_customer_id IN NUMBER,
p_product_id IN NUMBER,
p_quantity IN NUMBER
)
IS
v_stock_quantity NUMBER(10);
v_price NUMBER(10,2);
v_order_id NUMBER(10);
BEGIN
-- 获取商品价格
v_price := get_product_price(p_product_id);

-- 获取商品库存数量
SELECT stock_quantity INTO v_stock_quantity FROM inventory WHERE product_id = p_product_id;

-- 如果库存足够，则插入订单详情和更新库存数量
IF v_stock_quantity >= p_quantity THEN
-- 生成订单ID
SELECT order_seq.NEXTVAL INTO v_order_id FROM DUAL;

-- 插入订单
INSERT INTO orders(order_id, customer_id, order_date, total_price)
VALUES(v_order_id, p_customer_id, SYSDATE, v_price * p_quantity);

-- 插入订单详情
INSERT INTO order_details(order_id, product_id, quantity, price)
VALUES(v_order_id, p_product_id, p_quantity, v_price);

-- 更新库存
UPDATE inventory SET stock_quantity = stock_quantity - p_quantity WHERE product_id = p_product_id;
ELSE
RAISE_APPLICATION_ERROR(-20001, '库存不足');
END IF;
END;

<!-- 测试 -->
命令行中执行以下命令创建存储过程：

CREATE OR REPLACE PROCEDURE place_order_test AS
BEGIN
  place_order(1, 1001, 2); -- 测试库存足够的情况
  DBMS_OUTPUT.PUT_LINE('测试通过1');
  place_order(1, 1002, 20); -- 测试库存不足的情况
  DBMS_OUTPUT.PUT_LINE('测试通过2');
END;
这个存储过程会依次执行两次place_order存储过程，其中第一次会通过，第二次会抛出异常。

执行存储过程：
BEGIN
  place_order_test;
END;
```
![下单](./下单.JPG)
![下单测试](./下单测试.JPG)
- 下单后更新商品销售数量
``` 
CREATE OR REPLACE TRIGGER trg_update_product_sales_qty
AFTER INSERT ON order_details
FOR EACH ROW
BEGIN
UPDATE products SET sales_quantity = sales_quantity - :new.quantity WHERE product_id = :new.product_id;
END;

```
![触发器](./触发器.JPG)
![最后应该](./最后应该.JPG)

### 7.备份方案
最后，我们需要设计一套备份方案，以确保系统数据的安全性和可恢复性。我们可以使用Oracle的备份和恢复工具RMAN来创建备份。以下是备份的基本步骤：

使用sqlplus命令以sysdba用户身份登录到Oracle数据库，该命令如下：

```
 sqlplus / as sysdba
```
![备份第一步](./备份第一步.JPG)
关闭数据库并以MOUNT模式重新启动数据库，该命令如下：
```
SHUTDOWN IMMEDIATE
STARTUP MOUNT
```
![备份第二步](./备份第二步.JPG)
将数据库切换到归档模式，该命令如下：
```
ALTER DATABASE ARCHIVELOG;
```
![备份第三步](./备份第三步.JPG)
将数据库打开，该命令如下：
```
ALTER DATABASE OPEN;
```
![备份第四步](./备份第四步.JPG)
使用rman命令以目标用户身份连接到Oracle数据库，该命令如下：
```
rman target /
```
![备份第五步](./备份第五步.JPG)
显示rman的配置信息，该命令如下：
```
 SHOW ALL;
```
![备份第六步](./备份第六步.JPG)
对整个数据库执行备份，该命令如下：
```
BACKUP DATABASE;
```
![备份第七步](./备份第七步.JPG)
列出备份信息，该命令如下：
```
LIST BACKUP;
```
![备份完成页](./备份完成页.JPG)
## 总结
本次实验中，我们学习了如何使用 Oracle 数据库创建用户、表空间、表和存储过程/函数，并且设计了一套数据库备份方案。关于实验中的一些细节，我会逐一进行说明。

首先，我们创建了两个不同的表空间，一个用于存放数据，另一个用于存放索引。这样可以提高数据库查询效率。然后，我们创建了两个不同的用户，一个名为 admin 用于管理员数据库，另一个名为 user1 用于执行业务逻辑。在为用户分配权限时，我们授予 admin 用户 DBA 角色，即数据库管理员角色，而给 user1 用户授予 connect 和 resource 角色，以及访问表的权限，使其能够操作数据库。

接着，我们创建了四张表来模拟商品销售系统，即商品表、订单表、订单明细表和客户表。在创建存储过程和函数时，我们定义了一个名为 sales_pkg 的程序包，并在其中实现了两个存储过程：place_order 和 get_product_inventory。其中，place_order 存储过程用于在订单表和订单明细表中创建新订单记录，并更新产品的库存量。而 get_product_inventory 函数用于获取指定产品的库存量。

最后，我们设计了一套数据库备份方案，以确保数据安全。备份策略包括每天备份一次完整数据库并将备份文件保存在另一台服务器上，每小时备份一次日志并将备份文件保存在本地硬盘上，以及每周备份一次表空间。我们使用 Oracle 数据库自带的 RMAN 工具来实现备份。

总的来说，本次实验涉及到了许多 Oracle 数据库的基础操作，包括数据库对象的创建、用户权限的管理、SQL 语句的编写、PL/SQL 存储过程和函数的定义、以及数据库备份和恢复等方面。这些都是非常重要的数据库管理技能，对于我来说，是非常重要的知识。



